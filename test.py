import matplotlib.pyplot as plt

# Read all file
myfile = open('ecg_data.dat', 'r')
data  = myfile.readlines()
myfile.close()

# Split to channels
channel = []
for line in data:
  # Convert 'str' to 'int'
  tmp = [int(i) for i in line.split()]
  channel.append(tmp)

# Debug
print('First 20 samples from channel 0:')
print()
print(channel[0][0:20])
print()
print('First 20 samples from channel 1:')
print()
print(channel[1][0:20])

# Read acc. files
myfile = open('ecg_acc.dat', 'r')
acc_data = myfile.readline()
myfile.close()

# Get accelerometer data
acc_data = acc_data.split()
print('First 20 accelerometer samples:')
print()
print(acc_data[0:20])

# Draw
pPLT_MIN = 10000
pPLT_MAX = 15000

plt.figure(1)
plt.subplot(211)
for plot in channel:
  plt.plot(plot[pPLT_MIN:pPLT_MAX])
plt.show()
